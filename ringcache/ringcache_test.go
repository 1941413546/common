/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ringcache

import (
	"fmt"
	"testing"

	"github.com/test-go/testify/require"
)

func TestRingCache(t *testing.T) {
	ringCache := NewRingCache(uint(10))
	for i := 0; i < 10; i++ {
		key := fmt.Sprintf("key_%d", i)
		val := fmt.Sprintf("val_%d", i)
		require.Equal(t, true, ringCache.Put(key, val))
		require.Equal(t, val, ringCache.Get(key).(string))
		require.Equal(t, i+1, ringCache.Size())
	}
	for i := 0; i < 10; i++ {
		key := fmt.Sprintf("key_%d", i)
		val := fmt.Sprintf("val_%d", i)
		require.Equal(t, false, ringCache.Put(key, val))
		require.Equal(t, val, ringCache.Get(key).(string))
		require.Equal(t, 10, ringCache.Size())
	}
	for i := 10; i < 20; i++ {
		key := fmt.Sprintf("key_%d", i)
		require.Equal(t, nil, ringCache.Get(key))
	}
}
