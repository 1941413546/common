/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ringcache

import "sync"

type RingCache struct {
	sync.RWMutex
	ring  []string
	cache map[string]interface{}
	idx   uint
	cap   uint
}

func NewRingCache(cap uint) *RingCache {
	return &RingCache{
		ring:  make([]string, cap),
		cache: make(map[string]interface{}, cap),
		idx:   0,
		cap:   cap,
	}
}

func (r *RingCache) Put(key string, val interface{}) bool {
	r.Lock()
	defer r.Unlock()
	if _, exist := r.cache[key]; exist {
		return false
	}
	if existKey := r.ring[r.idx]; existKey != "" {
		delete(r.cache, existKey)
	}
	r.ring[r.idx] = key
	r.cache[key] = val
	r.idx = (r.idx + 1) % r.cap
	return true
}

func (r *RingCache) Get(key string) interface{} {
	r.RLock()
	defer r.RUnlock()
	if val, exist := r.cache[key]; exist && val != nil {
		return val
	}
	return nil
}

func (r *RingCache) Size() int {
	r.RLock()
	defer r.RUnlock()
	return len(r.cache)
}
