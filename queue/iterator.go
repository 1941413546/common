/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package queue

// Iterator 迭代器
type Iterator struct {
	current *node
}

// Value 获得Value值
// @return Element
func (i *Iterator) Value() Element {
	if i.current == nil {
		return nil
	}
	return i.current.value
}

// Next 下一个迭代器值
// @return *Iterator
func (i *Iterator) Next() *Iterator {
	if i.current == nil {
		return nil
	}
	i.current = i.current.next
	return i
}
