package ethbase

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHashIsZero(t *testing.T) {
	h1 := StringToHash("")
	assert.True(t, h1.IsZero())
	h2 := Hash{}
	assert.True(t, h2.IsZero())
}
