package ethbase

import (
	"encoding/hex"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUnmarshalEthTransferTx(t *testing.T) {
	rawTx, _ := hex.DecodeString("f86c0d85028fa6ae0082520894faf51dfa3b8ba59844467ddc13ba6b7d0322f1078802c68af0bb1400008026a0bbc809446cbb959c38b4fe34f94c01baefc7eaba62f08bc6cfb957bb34387bafa00213d7e4160921e956e46eb494bfab34106a3cc9bc9b26e62dc4317d3236e82c")
	//随便找了一个真实的转账Tx来测试 0xcb99eee79470c5935da1ff003237a91d3461fe2543687475232c298c864d3fd7
	ethTx := &Transaction{}
	err := ethTx.UnmarshalRLP(rawTx)
	assert.NoError(t, err)
	assert.EqualValues(t, 13, ethTx.GetNonce())
	assert.Equal(t, uint64(11*1000000000), ethTx.GetGasPrice().Uint64())
	assert.Equal(t, uint64(21000), ethTx.GetGas())
	assert.Equal(t, "0xfaf51dfa3b8ba59844467ddc13ba6b7d0322f107", strings.ToLower(ethTx.GetTo().String()))
	assert.EqualValues(t, 200000000000000000, ethTx.GetValue().Uint64()) //0.2ETH
	assert.Equal(t, 0, len(ethTx.GetData()))
	pk, _ := ethTx.GetSignerPubKey()
	pkBytes := MarshalPublicKey(pk)
	t.Logf("pubkey:%x,len:%d", pkBytes, len(pkBytes))
	assert.Equal(t, "0xcb99eee79470c5935da1ff003237a91d3461fe2543687475232c298c864d3fd7", ethTx.GetHash().String())

	signer, err := ethTx.GetSigner()
	assert.NoError(t, err)
	assert.Equal(t, "0xf6ebcdb018f1d4e1e9197024090149e95270a1e3", strings.ToLower(signer.String()))

	t.Log("ChainID:", ethTx.GetChainID())

	assert.EqualValues(t, rawTx, ethTx.MarshalRLP())

}

func TestUnmarshalEthCreateContractTx(t *testing.T) {
	rawTx, _ := hex.DecodeString("f8d483394fa18459682f05830186a08080b8807fcc2e0e531aa9990a238480ba078962aba418b68e658c576ae96b7a46475018766017527f674d62cc465602c5eeb18f50a748bc552839b5980186928879c4e8e2fcff45e06037527f725da5826d6d7983bb4a330f638fb136ddcd1acb349e3a706221f0dbeba289276057527fea0460cd591a3c9f765e3f3a9dfbd21bbf1afe29a09dce420068681aaed6bb859a8de306d58c28a49b8b611447c0604054af2d4a07a0218b8bded30a1a9c4874f31a25d0f0170ecb0f7ce9f192e87a46d7091c49fe30")
	//随便找了一个Ropsten真实的转账Tx来测试 0x09d053f202a32d0c1ed49c9c0ce9422c737f6a2dcec37e0f251b2d86e71893ad
	ethTx := &Transaction{}
	err := ethTx.UnmarshalRLP(rawTx)
	assert.NoError(t, err)
	assert.EqualValues(t, 3755937, ethTx.GetNonce())
	assert.Equal(t, uint64(1500000005), ethTx.GetGasPrice().Uint64())
	assert.Equal(t, uint64(100000), ethTx.GetGas())
	assert.Nil(t, ethTx.GetTo())                        //合约创建，To为空
	assert.EqualValues(t, 0, ethTx.GetValue().Uint64()) //0ETH
	//assert.Equal(t, 0, len(ethTx.GetData()))
	t.Logf("ContractByteCode:%x", ethTx.GetData())
	assert.Equal(t, "0x09d053f202a32d0c1ed49c9c0ce9422c737f6a2dcec37e0f251b2d86e71893ad", ethTx.GetHash().String())

	signer, err := ethTx.GetSigner()
	assert.NoError(t, err)
	assert.Equal(t, "0x2ca5f489cc1fd1cec24747b64e8de0f4a6a850e1", strings.ToLower(signer.String()))
	assert.EqualValues(t, 3, ethTx.GetChainID().Uint64())
	t.Log("Ropsten ChainID:", ethTx.GetChainID())
	assert.EqualValues(t, rawTx, ethTx.MarshalRLP())

}

func TestUnmarshalEthInvokeContractTx(t *testing.T) {
	rawTx, _ := hex.DecodeString("f8aa820d2a8318c5778308d33e9407865c6e87b9f70255377e024ace6630c1eaa37f80b844095ea7b300000000000000000000000050a61eb5a98b78fddec5eb2240e7b3b1fd62e162000000000000000000000000000000000000000000000000000000000000014d2da0fe43efb85daf527ed0c8692facce0e6124f4d03bb51a5ab5105efee44858b5e9a0012f31b3e6899124bfd68f281548914d3418dfb7de6c93b8b62ac5339b5f98f9")
	//随便找了一个Goerli真实的转账Tx来测试 0xf3794720cb7601e3c94a4846250fc0c629a5a74820af85b4ee0905d83ce7d475
	ethTx := &Transaction{}
	err := ethTx.UnmarshalRLP(rawTx)
	assert.NoError(t, err)
	assert.EqualValues(t, 3370, ethTx.GetNonce())
	assert.Equal(t, uint64(1623415), ethTx.GetGasPrice().Uint64())
	assert.Equal(t, uint64(578366), ethTx.GetGas())
	assert.Equal(t, "0x07865c6e87b9f70255377e024ace6630c1eaa37f", strings.ToLower(ethTx.GetTo().String()))
	assert.EqualValues(t, 0, ethTx.GetValue().Uint64()) //0ETH
	//assert.Equal(t, 0, len(ethTx.GetData()))
	t.Logf("InvokeData:%x", ethTx.GetData())
	assert.Equal(t, "0xf3794720cb7601e3c94a4846250fc0c629a5a74820af85b4ee0905d83ce7d475", ethTx.GetHash().String())

	signer, err := ethTx.GetSigner()
	assert.NoError(t, err)
	assert.Equal(t, "0xd5045deea369d64ab7efab41ad18b82ee165ebaa", strings.ToLower(signer.String()))
	assert.EqualValues(t, 5, ethTx.GetChainID().Uint64())
	t.Log("Goerli ChainID:", ethTx.GetChainID())
	assert.EqualValues(t, rawTx, ethTx.MarshalRLP())

}

func TestUnmarshalEthTransferTx2(t *testing.T) {
	rawTx, _ := hex.DecodeString("02f874830138818084ca5b96c084ca5b96dd82520894f5923a67697da614f0e8ae25cfefc590122561c187303e074f67b00080c080a01720865fd0093e1048ce5aebff2db755c4d94dc84a61a92afc58cb75a449eefba06b7ec6c1a5b1fe95e0c55f40e7b467fe2b7a8b21abd80a7f864cfd0957b3c66c")
	//随便找了一个Polygon PoS Testnet真实的转账Tx来测试 0x11103c2414e5a487150c4ecaf60d78f9610b9e6e3b829113cacfacc5056ac034
	// https://api-testnet.polygonscan.com/api?module=proxy&action=eth_getTransactionByHash&txhash=0x11103c2414e5a487150c4ecaf60d78f9610b9e6e3b829113cacfacc5056ac034&apikey=YourApiKeyToken
	ethTx := &Transaction{}
	err := ethTx.UnmarshalRLP(rawTx)
	assert.NoError(t, err)
	assert.EqualValues(t, 80001, ethTx.GetChainID().Uint64())
	t.Log("ChainID:", ethTx.GetChainID())
	assert.EqualValues(t, 0, ethTx.GetNonce())
	assert.Equal(t, uint64(3395000029), ethTx.GetGasFeeCap().Uint64())
	assert.Equal(t, uint64(3395000000), ethTx.GetGasTipCap().Uint64())
	assert.Equal(t, uint64(21000), ethTx.GetGas())
	assert.Equal(t, "0xf5923a67697da614f0e8ae25cfefc590122561c1", strings.ToLower(ethTx.GetTo().String()))
	assert.EqualValues(t, 13579000000000000, ethTx.GetValue().Uint64()) //0.013579ETH
	assert.Equal(t, 0, len(ethTx.GetData()))

	assert.Equal(t, "0x11103c2414e5a487150c4ecaf60d78f9610b9e6e3b829113cacfacc5056ac034", ethTx.GetHash().String())
	r, s, v := ethTx.GetRawSignatureValues()
	t.Logf("R:%x,S:%x,V:%x", r, s, v)
	signer, err := ethTx.GetSigner()
	assert.NoError(t, err)
	assert.Equal(t, "0x2fe755f4f649ac934955649aced81cf3acac2e51", strings.ToLower(signer.String()))
	assert.EqualValues(t, rawTx, ethTx.MarshalRLP())

}
func TestUnmarshalRlp2Tx(t *testing.T) {
	data, _ := hex.DecodeString("f86d80843b9aca00825208942fe755f4f649ac934955649aced81cf3acac2e51881121d335973840008082603da0ae6129c5d786667a0c95c3ad4156b7c59ba86bf630b99f39d3c6c78c36e7daeda06da4ad0245e745b7d98ec96166de64c2528e164c4c37251c63165f94058d504a")
	ethTx := &Transaction{}
	err := ethTx.UnmarshalRLP(data)
	assert.NoError(t, err)
	t.Log(ethTx.ToString())
	t.Logf("TxHashString:%s \tTxHashBytes:%x", ethTx.GetHash().String(), ethTx.GetHash().Bytes())
	t.Log(hex.EncodeToString(ethTx.GetHash().Bytes()))
}
