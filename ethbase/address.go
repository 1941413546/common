/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"encoding/hex"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"unicode"

	"chainmaker.org/chainmaker/common/v3/ethbase/keccak"
)

var errInvalidAddress = errors.New("invalid address")

// ZeroAddress 空地址
var ZeroAddress = Address{}

const (
	AddressLength = 20
)

// EncodeToHex 将二进制转换为以太坊格式的Hex
// @param str
// @return string
func EncodeToHex(str []byte) string {
	return "0x" + hex.EncodeToString(str)
}

// DecodeHex converts a hex string to a byte array
func DecodeHex(str string) ([]byte, error) {
	str = strings.TrimPrefix(str, "0x")

	return hex.DecodeString(str)
}

// Address  以太坊地址
type Address [AddressLength]byte

func min(i, j int) int {
	if i < j {
		return i
	}

	return j
}

// checksumEncode returns the checksummed address with 0x prefix, as by EIP-55
// https://github.com/ethereum/EIPs/blob/master/EIPS/eip-55.md
func (a Address) checksumEncode() string {
	addrBytes := a.Bytes() // 20 bytes

	// Encode to hex without the 0x prefix
	lowercaseHex := EncodeToHex(addrBytes)[2:]
	hashedAddress := EncodeToHex(keccak.Keccak256(nil, []byte(lowercaseHex)))[2:]

	result := make([]rune, len(lowercaseHex))
	// Iterate over each character in the lowercase hex address
	for idx, ch := range lowercaseHex {
		if ch >= '0' && ch <= '9' || hashedAddress[idx] >= '0' && hashedAddress[idx] <= '7' {
			// Numbers in range [0, 9] are ignored (as well as hashed values [0, 7]),
			// because they can't be uppercased
			result[idx] = ch
		} else {
			// The current character / hashed character is in the range [8, f]
			result[idx] = unicode.ToUpper(ch)
		}
	}

	return "0x" + string(result)
}

// Ptr 地址的指针形式
// @return *Address
func (a Address) Ptr() *Address {
	return &a
}

// String 地址的字符串形式,遵从EIP55标准，有大小写区分
// @return string
func (a Address) String() string {
	return a.checksumEncode()
}

// StringSimple 地址的字符串形式，全是小写
// @return string
func (a Address) StringSimple() string {
	return EncodeToHex(a[:])
}

// Bytes 地址的二进制形式
// @return []byte
func (a Address) Bytes() []byte {
	return a[:]
}

//func (a *Address) Scan(src interface{}) error {
//	stringVal, ok := src.([]byte)
//	if !ok {
//		return errors.New("invalid type assert")
//	}
//
//	aa, decodeErr := DecodeHex(string(stringVal))
//	if decodeErr != nil {
//		return fmt.Errorf("unable to decode value, %w", decodeErr)
//	}
//
//	copy(a[:], aa[:])
//
//	return nil
//}

// StringToAddress 忽略错误的可能，直接将字符串转换为地址
// @param str
// @return Address
func StringToAddress(str string) Address {
	return BytesToAddress(stringToBytes(str))
}

// BytesToAddress 将地址20字节转换为地址类型
// @param b
// @return Address
func BytesToAddress(b []byte) Address {
	var a Address

	size := len(b)
	min := min(size, AddressLength)

	copy(a[AddressLength-min:], b[len(b)-min:])

	return a
}

func stringToBytes(str string) []byte {
	str = strings.TrimPrefix(str, "0x")
	if len(str)%2 == 1 {
		str = "0" + str
	}

	b, _ := hex.DecodeString(str)

	return b
}

var ethAddrReg = regexp.MustCompile(`^(0x)?[a-fA-F0-9]{40}$`)

// ParseAddress 解析字符串成以太坊地址
// @param str 必须以0x开头的40字节Hex或者就是40字节的Hex
// @return Address
// @return error
func ParseAddress(str string) (Address, error) {
	if ethAddrReg.MatchString(str) {
		return StringToAddress(str), nil
	}
	return ZeroAddress, errInvalidAddress
}

// ParseAddressStrict 解析以太坊地址，严格检查格式包括EIP-55大小写要求，而且必须0x开头
// @param str
// @return Address
// @return error
func ParseAddressStrict(str string) (Address, error) {
	addr, err := ParseAddress(str)
	if err != nil {
		return addr, err
	}
	if addr.String() == str {
		return addr, nil
	}
	return ZeroAddress, errInvalidAddress
}

// UnmarshalText parses an address in hex syntax.
func (a *Address) UnmarshalText(input []byte) error {
	buf := stringToBytes(string(input))
	if len(buf) != AddressLength {
		return fmt.Errorf("incorrect length")
	}

	*a = BytesToAddress(buf)

	return nil
}
func (a *Address) UnmarshalJSON(input []byte) error {
	if len(input) == 0 {
		return errors.New("input is empty")
	}
	if input[0] == '"' {
		input = input[1 : len(input)-1]
	}

	addr, err := ParseAddress(string(input))
	if err != nil {
		return err
	}
	*a = addr
	return nil
}

// MarshalText 地址序列化字符串
// @return []byte
// @return error
func (a Address) MarshalText() ([]byte, error) {
	return []byte(a.String()), nil
}

func (a *Address) MarshalJSON() ([]byte, error) {
	addrStr := a.String()
	return []byte("\"" + addrStr + "\""), nil
}

// IsZero 是否空地址
// @return bool
func (a Address) IsZero() bool {
	return a == ZeroAddress
}

// IsZeroAddress 判断是否空地址
// @param addr
// @return bool
func IsZeroAddress(addr string) bool {
	if len(addr) == 0 {
		return true
	}
	a, _ := ParseAddress(addr)
	return a.IsZero()
}
