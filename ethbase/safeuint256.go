/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"errors"
	"math/big"
)

// SafeUint256 calculation uint256
type SafeUint256 big.Int

var (
	// SafeUintOne is 1
	SafeUintOne    = (*SafeUint256)(big.NewInt(1))
	maxSafeUint256 *SafeUint256
	minSafeUint256 *SafeUint256
	SafeMathZero   *SafeUint256
)

func init() {
	x := big.NewInt(1)
	x = x.Lsh(x, 256).Sub(x, big.NewInt(1))
	maxSafeUint256 = (*SafeUint256)(x)
	minSafeUint256 = (*SafeUint256)(big.NewInt(0))
	SafeMathZero = (*SafeUint256)(big.NewInt(0))
}

// ParseSafeUint256 get uint256 obj from str
func ParseSafeUint256(x string) (*SafeUint256, bool) {
	z := big.NewInt(0)
	if x == "" {
		return (*SafeUint256)(z), true
	}
	z, ok := z.SetString(x, 10)
	if !ok || z.Cmp((*big.Int)(maxSafeUint256)) > 0 || z.Cmp((*big.Int)(minSafeUint256)) < 0 {
		return nil, false
	}
	return (*SafeUint256)(z), true
}

// NewSafeUint256 基于字节数组创建SafeUint256
// @param x
// @return *SafeUint256
// @return bool
func NewSafeUint256(x []byte) (*SafeUint256, bool) {
	z := big.NewInt(0)
	z = z.SetBytes(x)
	return (*SafeUint256)(z), true
}

// NewSafeUint256FromUint64 基于Uint64创建SafeUint256
// @param i
// @return *SafeUint256
func NewSafeUint256FromUint64(i uint64) *SafeUint256 {
	z := big.NewInt(0)
	z = z.SetUint64(i)
	return (*SafeUint256)(z)
}

// SafeAdd sets z to the sum x+y and returns z.
func SafeAdd(x, y *SafeUint256) (*SafeUint256, bool) {
	z := big.NewInt(0)
	z = z.Add((*big.Int)(x), (*big.Int)(y))
	if z.Cmp((*big.Int)(maxSafeUint256)) > 0 {
		return nil, false
	}
	return (*SafeUint256)(z), true
}

// SafeSub sets z to the difference x-y and returns z.
func SafeSub(x, y *SafeUint256) (*SafeUint256, bool) {
	if (*big.Int)(x).Cmp((*big.Int)(y)) < 0 {
		return nil, false
	}
	return (*SafeUint256)((*big.Int)(x).Sub((*big.Int)(x), (*big.Int)(y))), true
}

// SafeMul sets z to the product x*y and returns z.
func SafeMul(x, y *SafeUint256) (*SafeUint256, bool) {
	z := (*big.Int)(x).Mul((*big.Int)(x), (*big.Int)(y))
	if z.Cmp((*big.Int)(maxSafeUint256)) > 0 || z.Cmp((*big.Int)(minSafeUint256)) < 0 {
		return nil, false
	}
	return (*SafeUint256)(z), true
}

// SafeDiv sets z to the quotient x/y for y != 0 and returns z.
// If y == 0, a division-by-zero run-time panic occurs.
// Div implements Euclidean division (unlike Go); see DivMod for more details.
func SafeDiv(x, y *SafeUint256) *SafeUint256 {
	return (*SafeUint256)((*big.Int)(x).Div((*big.Int)(x), (*big.Int)(y)))
}

// Cmp compares x and y and returns:
//
//   -1 if x <  y
//    0 if x == y
//   +1 if x >  y
//
func (x *SafeUint256) Cmp(y *SafeUint256) int {
	return (*big.Int)(x).Cmp((*big.Int)(y))
}

// ToString get str from uint256
func (x *SafeUint256) ToString() string {
	return (*big.Int)(x).String()
}
func (x *SafeUint256) Bytes() []byte {
	return (*big.Int)(x).Bytes()
}

// IsMaxSafeUint256 return is maxUint256
func (x *SafeUint256) IsMaxSafeUint256() bool {
	return x.Cmp(maxSafeUint256) == 0
}
func (x *SafeUint256) IsZero() bool {
	return x.ToString() == "0"
}

func (a *SafeUint256) MarshalJSON() ([]byte, error) {
	str := "\"" + a.ToString() + "\""
	return []byte(str), nil
}
func (a *SafeUint256) UnmarshalJSON(input []byte) error {
	if len(input) == 0 {
		return errors.New("input is empty")
	}
	if input[0] == '"' {
		input = input[1 : len(input)-1]
	}
	x, ok := ParseSafeUint256(string(input))
	if !ok {
		return errors.New("invalid SafeUint256 string input")
	}
	*a = *x
	return nil
}
