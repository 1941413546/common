/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"fmt"
	"math/big"

	"chainmaker.org/chainmaker/common/v3/ethbase/keccak"
	"chainmaker.org/chainmaker/common/v3/json"
	"github.com/umbracle/fastrlp"
)

// DynamicFeeTx EIP-1559 Tx
type DynamicFeeTx struct {
	ChainID              *big.Int
	Nonce                uint64
	MaxPriorityFeePerGas *big.Int
	MaxFeePerGas         *big.Int
	Gas                  uint64
	To                   *Address // nil means contract creation
	Value                *big.Int
	Input                []byte
	AccessList           AccessList

	// Signature values
	V *big.Int
	R *big.Int
	S *big.Int
}

func (t *DynamicFeeTx) ComputeHash() Hash {
	data := t.MarshalRLP()
	return BytesToHash(Keccak256(data))
}

func (t *DynamicFeeTx) GetSignatureYParity() byte {
	return t.V.Bytes()[0]
}

func (t *DynamicFeeTx) GetSignature() ([]byte, error) {
	v := byte(0)
	if len(t.V.Bytes()) > 0 {
		v = t.V.Bytes()[0]
	}
	return EncodeSignature(t.R, t.S, v)
}

func (t *DynamicFeeTx) GetTxType() TxType {
	return DynamicFeeTxType
}

func (t *DynamicFeeTx) GetChainID() *big.Int {
	return t.ChainID
}

func (t *DynamicFeeTx) GetAccessList() AccessList {
	return t.AccessList
}

func (t *DynamicFeeTx) GetData() []byte {
	return t.Input
}

func (t *DynamicFeeTx) GetGas() uint64 {
	return t.Gas
}

func (t *DynamicFeeTx) GetGasPrice() *big.Int {
	return t.MaxFeePerGas
}

func (t *DynamicFeeTx) GetGasTipCap() *big.Int {
	return t.MaxPriorityFeePerGas
}

func (t *DynamicFeeTx) GetGasFeeCap() *big.Int {
	return t.MaxFeePerGas
}

func (t *DynamicFeeTx) GetValue() *big.Int {
	return t.Value
}

func (t *DynamicFeeTx) GetNonce() uint64 {
	return t.Nonce
}

func (t *DynamicFeeTx) GetTo() *Address {
	return t.To
}
func (t *DynamicFeeTx) GetRawSignatureValues() (r, s, v *big.Int) {
	return t.R, t.S, t.V
}

// AccessList is an EIP-2930 access list.
type AccessList []AccessTuple

func (a *AccessList) MarshalRLPWith(arena *fastrlp.Arena) (*fastrlp.Value, error) {
	if len(*a) == 0 {
		return arena.NewNullArray(), nil
	}
	v := arena.NewArray()
	for _, i := range *a {
		acct := arena.NewArray()
		acct.Set(arena.NewCopyBytes(i.Address[:]))
		if len(i.StorageKeys) == 0 {
			acct.Set(arena.NewNullArray())
		} else {
			strV := arena.NewArray()
			for _, v := range i.StorageKeys {
				strV.Set(arena.NewCopyBytes(v[:]))
			}
			acct.Set(strV)
		}
		v.Set(acct)
	}
	return v, nil
}

// AccessTuple is the element type of an access list.
type AccessTuple struct {
	Address     Address `json:"address"        gencodec:"required"`
	StorageKeys []Hash  `json:"storageKeys"    gencodec:"required"`
}

func (t *DynamicFeeTx) UnmarshalRLP(input []byte) error {
	return UnmarshalRlp(t.UnmarshalRLPFrom, input)
}

// UnmarshalRLPFrom unmarshals
//0x02 || RLP([chainId, nonce, maxPriorityFeePerGas, maxFeePerGas, gasLimit, to, value, data, accessList, signatureYParity, signatureR, signatureS])
func (t *DynamicFeeTx) UnmarshalRLPFrom(p *fastrlp.Parser, v *fastrlp.Value) error {
	elems, err := v.GetElems()
	if err != nil {
		return err
	}

	if len(elems) < 12 {
		return fmt.Errorf("incorrect number of elements to decode transaction, expected 12 but found %d", len(elems))
	}
	t.ChainID = new(big.Int)
	if err := elems[0].GetBigInt(t.ChainID); err != nil {
		return err
	}
	// nonce
	if t.Nonce, err = elems[1].GetUint64(); err != nil {
		return err
	}
	// gasPrice
	t.MaxPriorityFeePerGas = new(big.Int)
	if err := elems[2].GetBigInt(t.MaxPriorityFeePerGas); err != nil {
		return err
	}
	t.MaxFeePerGas = new(big.Int)
	if err := elems[3].GetBigInt(t.MaxFeePerGas); err != nil {
		return err
	}
	// gas
	if t.Gas, err = elems[4].GetUint64(); err != nil {
		return err
	}
	// to
	if vv, _ := v.Get(5).Bytes(); len(vv) == 20 {
		// address
		addr := BytesToAddress(vv)
		t.To = &addr
	} else {
		// reset To
		t.To = nil
	}
	// value
	t.Value = new(big.Int)
	if err := elems[6].GetBigInt(t.Value); err != nil {
		return err
	}
	// input
	if t.Input, err = elems[7].GetBytes(t.Input[:0]); err != nil {
		return err
	}
	//AccessList
	if acl, _ := elems[8].Bytes(); len(acl) > 0 {
		//TODO
	} else {
		t.AccessList = AccessList{}
	}

	// V
	t.V = new(big.Int)
	if err = elems[9].GetBigInt(t.V); err != nil {
		return err
	}

	// R
	t.R = new(big.Int)
	if err = elems[10].GetBigInt(t.R); err != nil {
		return err
	}
	// S
	t.S = new(big.Int)
	if err = elems[11].GetBigInt(t.S); err != nil {
		return err
	}

	return nil
}
func (t *DynamicFeeTx) MarshalRLP() []byte {
	return append([]byte{byte(2)}, t.MarshalRLPTo(nil)...)
}

func (t *DynamicFeeTx) MarshalRLPTo(dst []byte) []byte {
	return MarshalRLPTo(t.MarshalRLPWith, dst)
}

// MarshalRLPWith marshals the transaction to RLP with a specific fastrlp.Arena
func (tx *DynamicFeeTx) MarshalRLPWith(a *fastrlp.Arena) *fastrlp.Value {
	v := a.NewArray()
	v.Set(a.NewBigInt(tx.ChainID))
	v.Set(a.NewUint(tx.Nonce))
	v.Set(a.NewBigInt(tx.MaxPriorityFeePerGas))
	v.Set(a.NewBigInt(tx.MaxFeePerGas))
	v.Set(a.NewUint(tx.Gas))

	if tx.To == nil {
		v.Set(a.NewNull())
	} else {
		v.Set(a.NewCopyBytes((*tx.To).Bytes()))
	}

	v.Set(a.NewBigInt(tx.Value))
	v.Set(a.NewCopyBytes(tx.Input))
	accessList, err := tx.AccessList.MarshalRLPWith(a)
	if err != nil {
		panic(err)
	}
	v.Set(accessList)
	// signature values
	v.Set(a.NewBigInt(tx.V))
	v.Set(a.NewBigInt(tx.R))
	v.Set(a.NewBigInt(tx.S))

	return v
}
func (tx *DynamicFeeTx) GetUnsignedHash() Hash {
	m := func(a *fastrlp.Arena) *fastrlp.Value {
		v := a.NewArray()
		v.Set(a.NewBigInt(tx.ChainID))
		v.Set(a.NewUint(tx.Nonce))
		v.Set(a.NewBigInt(tx.MaxPriorityFeePerGas))
		v.Set(a.NewBigInt(tx.MaxFeePerGas))
		v.Set(a.NewUint(tx.Gas))

		if tx.To == nil {
			v.Set(a.NewNull())
		} else {
			v.Set(a.NewCopyBytes((*tx.To).Bytes()))
		}

		v.Set(a.NewBigInt(tx.Value))
		v.Set(a.NewCopyBytes(tx.Input))
		accessList, err := tx.AccessList.MarshalRLPWith(a)
		if err != nil {
			panic(err)
		}
		v.Set(accessList)
		return v
	}
	bytes := MarshalRLPTo(m, nil)
	bytes = append([]byte{byte(DynamicFeeTxType)}, bytes...)
	hash := keccak.Keccak256(nil, bytes)

	return BytesToHash(hash)
}
func (t *DynamicFeeTx) ToString() string {
	d, _ := json.Marshal(t)
	return string(d)
}
