/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"crypto/ecdsa"
	"errors"
	"math/big"

	"chainmaker.org/chainmaker/common/v3/ethbase/keccak"
)

type TxType uint8

const (
	LegacyTxType TxType = iota
	AccessListTxType
	DynamicFeeTxType
)

type Transaction struct {

	// bytes 序列化后的Bytes
	bytes []byte
	//innerTx 数字信封反解后的内部交易对象
	innerTx TxData
	//Hash 签名后的完整交易的Hash
	hash Hash
	//UnsignedHash 未签名时交易
	unsignedHash Hash
}

func (t *Transaction) GetTxType() TxType {
	return t.innerTx.GetTxType()
}

func (t *Transaction) GetChainID() *big.Int {
	return t.innerTx.GetChainID()
}

func (t *Transaction) GetAccessList() AccessList {
	return t.innerTx.GetAccessList()
}

func (t *Transaction) GetData() []byte {
	return t.innerTx.GetData()
}

func (t *Transaction) GetGas() uint64 {
	return t.innerTx.GetGas()
}

func (t *Transaction) GetGasPrice() *big.Int {
	return t.innerTx.GetGasPrice()
}

func (t *Transaction) GetGasTipCap() *big.Int {
	return t.innerTx.GetGasTipCap()
}

func (t *Transaction) GetGasFeeCap() *big.Int {
	return t.innerTx.GetGasFeeCap()
}

func (t *Transaction) GetValue() *big.Int {
	return t.innerTx.GetValue()
}

func (t *Transaction) GetNonce() uint64 {
	return t.innerTx.GetNonce()
}

func (t *Transaction) GetTo() *Address {
	return t.innerTx.GetTo()
}
func (t *Transaction) GetRawSignatureValues() (r, s, v *big.Int) {
	return t.innerTx.GetRawSignatureValues()
}

// GetSignature 获得RSV拼接后的签名数组，v已经去掉了chainId
// @return []byte
// @return error
func (t *Transaction) GetSignature() ([]byte, error) {
	return t.innerTx.GetSignature()
}
func (t *Transaction) GetSigner() (Address, error) {
	pub, err := t.GetSignerPubKey()
	if err != nil {
		return ZeroAddress, err
	}
	return PubKeyToAddress(pub), nil
}
func (t *Transaction) GetSignerPubKey() (*ecdsa.PublicKey, error) {
	sig, err := t.innerTx.GetSignature()
	if err != nil {
		return nil, err
	}
	txhash := t.innerTx.GetUnsignedHash().Bytes()
	pubKey, err := RecoverPubkey(sig, txhash)
	if err != nil {
		return nil, err
	}
	return pubKey, err
}

type TxData interface {
	GetTxType() TxType // returns the type ID

	GetChainID() *big.Int
	GetAccessList() AccessList
	GetData() []byte
	GetGas() uint64
	GetGasPrice() *big.Int
	GetGasTipCap() *big.Int
	GetGasFeeCap() *big.Int
	GetValue() *big.Int
	GetNonce() uint64
	GetTo() *Address
	UnmarshalRLP(b []byte) error
	// GetSignature 获得RSV拼接后的签名数组，v已经去掉了chainId
	// @return []byte
	// @return error
	GetSignature() ([]byte, error)
	GetUnsignedHash() Hash
	ComputeHash() Hash
	GetSignatureYParity() byte
	GetRawSignatureValues() (r, s, v *big.Int)
	MarshalRLP() []byte
	ToString() string
}

func NewTransaction(tx TxData) *Transaction {
	return &Transaction{innerTx: tx}
}
func (t *Transaction) GetHash() Hash {
	if t.hash.IsZero() {
		t.hash = t.innerTx.ComputeHash()
	}
	return t.hash
}
func (t *Transaction) UnmarshalRLP(b []byte) error {
	t.bytes = b
	if len(b) > 0 && b[0] > 0x7f {
		// It's a legacy transaction.
		tx := &LegacyTransaction{}
		err := tx.UnmarshalRLP(b)
		if err != nil {
			return err
		}
		t.innerTx = tx

		t.hash = tx.ComputeHash()
		return nil
	}
	// It's an EIP2718 typed transaction envelope.
	switch TxType(b[0]) {
	case AccessListTxType:
		//var inner AccessListTx
		//err := rlp.DecodeBytes(b[1:], &inner)
		//return &inner, err
	case DynamicFeeTxType:
		inner := &DynamicFeeTx{}
		err := inner.UnmarshalRLP(b[1:])
		if err != nil {
			return err
		}
		t.innerTx = inner
		t.hash = prefixedRlpHash(b)
	default:
		return errors.New("tx type not supported")
	}
	return nil
}

func (t *Transaction) MarshalRLP() []byte {
	return t.innerTx.MarshalRLP()
}

func prefixedRlpHash(b []byte) (h Hash) {
	sha := keccak.DefaultKeccakPool.Get()
	defer keccak.DefaultKeccakPool.Put(sha)
	sha.Reset()
	sha.Write(b)
	return BytesToHash(sha.Sum(nil))
}

func (t *Transaction) VerifySignature() (bool, error) {
	sig, err := t.innerTx.GetSignature()
	if err != nil {
		return false, err
	}
	txhash := t.innerTx.GetUnsignedHash().Bytes()
	_, err = Ecrecover(txhash, sig)
	if err != nil {
		return false, err
	}
	return true, nil
}
func (t *Transaction) ToString() string {
	return t.innerTx.ToString()
}
