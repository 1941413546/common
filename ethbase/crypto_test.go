/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"encoding/hex"
	"testing"

	"github.com/btcsuite/btcd/btcec"
	"github.com/stretchr/testify/assert"
)

func TestRecoverPubkey(t *testing.T) {

}

func TestCreateAddress(t *testing.T) {
	sender := StringToAddress("0x6ac7ea33f8831ea9dcc53393aaa88b25a785dbf0")
	assert.Equal(t, "0xcd234a471b72ba2f1ccf0a70fcaba648a5eecd8d",
		CreateAddress(sender, 0).StringSimple())
}

func TestCompressedPubKeyToAddress(t *testing.T) {
	pk, _ := hex.DecodeString("0380a373d14fd8d6e51aae948af5ba58e00f88bdad5d5869a240fb9a90d261293b")
	pubKey := UnmarshalPublicKey(pk)
	t.Logf("%#+v", pubKey)
	data1 := MarshalPublicKey(pubKey)
	pubKey2 := UnmarshalPublicKey(data1)
	t.Logf("%#+v", pubKey2)
	addr := PubKeyBytesToAddress(pk)
	pubkey3 := (*btcec.PublicKey)(pubKey)
	t.Logf("pubkey:%x", data1)
	t.Logf("pubkey:%x", pubkey3.SerializeUncompressed())

	assert.Equal(t, "0xcba472d0fe169e5632f217b3c00e9225165dea6e", addr.StringSimple())
}
