/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"encoding/json"
	"math/big"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseSafeUint256(t *testing.T) {
	type args struct {
		x string
	}
	tests := []struct {
		name  string
		args  args
		want  *SafeUint256
		want1 bool
	}{
		// TODO: Add test cases.
		{
			name: "empty string",
			args: args{
				x: "",
			},
			want:  (*SafeUint256)(big.NewInt(0)),
			want1: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := ParseSafeUint256(tt.args.x)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseSafeUint256() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("ParseSafeUint256() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestSafeUint256_MarshalJSON(t *testing.T) {
	a := NewSafeUint256FromUint64(uint64(123456789))
	j, err := json.Marshal(a)
	assert.NoError(t, err)
	t.Log(string(j))
	a2 := &SafeUint256{}
	err = json.Unmarshal(j, &a2)
	assert.NoError(t, err)
	t.Log(a2.ToString())
	assert.Equal(t, "123456789", a2.ToString())
}

type KV struct {
	K string
	V *SafeUint256
}

func TestSafeUint256Json(t *testing.T) {
	v, _ := ParseSafeUint256("987654321987654321987654321987654321")
	kv := &KV{
		K: "test",
		V: v,
	}
	j, _ := json.Marshal(kv)
	t.Log(string(j))
}
