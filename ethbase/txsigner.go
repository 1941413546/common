/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"fmt"
	"math/big"
	"math/bits"
)

// TxSigner is a utility interface used to recover data from a transaction
type TxSigner interface {
	// Hash returns the hash of the transaction
	Hash(tx *LegacyTransaction) Hash

	// Sender returns the sender of the transaction
	Sender(tx *LegacyTransaction) (Address, error)

	// CalculateV calculates the V value based on the type of signer used
	CalculateV(parity byte) []byte
}

const EIP155 = true

// NewSigner creates a new signer object (EIP155 or FrontierSigner)
func NewSigner(chainID uint64) TxSigner {
	var signer TxSigner

	if EIP155 {
		signer = &EIP155Signer{chainID: chainID}
	} else {
		signer = &FrontierSigner{}
	}

	return signer
}

type FrontierSigner struct {
}

// Hash is a wrapper function for the calcTxHash, with chainID 0
func (f *FrontierSigner) Hash(tx *LegacyTransaction) Hash {
	return calcTxHash(tx, 0)
}

// Magic numbers from Ethereum, used in v calculation
var (
	big27 = big.NewInt(27)
	big35 = big.NewInt(35)
)

// Sender decodes the signature and returns the sender of the transaction
func (f *FrontierSigner) Sender(tx *LegacyTransaction) (Address, error) {
	refV := big.NewInt(0)
	if tx.V != nil {
		refV.SetBytes(tx.V.Bytes())
	}

	refV.Sub(refV, big27)

	sig, err := EncodeSignature(tx.R, tx.S, byte(refV.Int64()))
	if err != nil {
		return ZeroAddress, err
	}

	pub, err := Ecrecover(f.Hash(tx).Bytes(), sig)
	if err != nil {
		return ZeroAddress, err
	}

	buf := Keccak256(pub[1:])[12:]

	return BytesToAddress(buf), nil
}

// calculateV returns the V value for transactions pre EIP155
func (f *FrontierSigner) CalculateV(parity byte) []byte {
	reference := big.NewInt(int64(parity))
	reference.Add(reference, big27)

	return reference.Bytes()
}

// NewEIP155Signer returns a new EIP155Signer object
func NewEIP155Signer(chainID uint64) *EIP155Signer {
	return &EIP155Signer{chainID: chainID}
}

type EIP155Signer struct {
	chainID uint64
}

// Hash is a wrapper function that calls calcTxHash with the EIP155Signer's chainID
func (e *EIP155Signer) Hash(tx *LegacyTransaction) Hash {
	return calcTxHash(tx, e.chainID)
}

// Sender returns the transaction sender
func (e *EIP155Signer) Sender(tx *LegacyTransaction) (Address, error) {
	protected := true

	// Check if v value conforms to an earlier standard (before EIP155)
	bigV := big.NewInt(0)
	if tx.V != nil {
		bigV.SetBytes(tx.V.Bytes())
	}

	if vv := bigV.Uint64(); bits.Len(uint(vv)) <= 8 {
		protected = vv != 27 && vv != 28
	}

	if !protected {
		return (&FrontierSigner{}).Sender(tx)
	}

	// Reverse the V calculation to find the original V in the range [0, 1]
	// v = CHAIN_ID * 2 + 35 + {0, 1}
	mulOperand := big.NewInt(0).Mul(big.NewInt(int64(e.chainID)), big.NewInt(2))
	bigV.Sub(bigV, mulOperand)
	bigV.Sub(bigV, big35)

	sig, err := EncodeSignature(tx.R, tx.S, byte(bigV.Int64()))
	if err != nil {
		return ZeroAddress, err
	}

	pub, err := Ecrecover(e.Hash(tx).Bytes(), sig)
	if err != nil {
		return ZeroAddress, err
	}

	buf := Keccak256(pub[1:])[12:]

	return BytesToAddress(buf), nil
}

// calculateV returns the V value for transaction signatures. Based on EIP155
func (e *EIP155Signer) CalculateV(parity byte) []byte {
	reference := big.NewInt(int64(parity))
	reference.Add(reference, big35)

	mulOperand := big.NewInt(0).Mul(big.NewInt(int64(e.chainID)), big.NewInt(2))

	reference.Add(reference, mulOperand)

	return reference.Bytes()
}

// EncodeSignature generates a signature value based on the R, S and V value
func EncodeSignature(R, S *big.Int, V byte) ([]byte, error) {
	if !ValidateSignatureValues(V, R, S) {
		return nil, fmt.Errorf("invalid txn signature")
	}

	sig := make([]byte, 65)
	copy(sig[32-len(R.Bytes()):32], R.Bytes())
	copy(sig[64-len(S.Bytes()):64], S.Bytes())
	sig[64] = V

	return sig, nil
}

func DecodeSignature(sig []byte) (r, s, v *big.Int) {
	if len(sig) != 65 {
		panic(fmt.Sprintf("wrong size for signature: got %d, want %d", len(sig), 65))
	}
	r = new(big.Int).SetBytes(sig[:32])
	s = new(big.Int).SetBytes(sig[32:64])
	v = new(big.Int).SetBytes([]byte{sig[64]})
	return r, s, v
}
