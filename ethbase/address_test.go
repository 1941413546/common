/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseAddress(t *testing.T) {
	testcase := map[string]bool{
		"0x388C818CA8B9251b393131C08a736A67ccB19297": true,
		"0x388C818CA8B9251b393131C08a736A67ccB1929":  false,
		"0x388C818CA8B9251b393131C08a736A67ccB1929x": false,
		"388C818CA8B9251b393131C08a736A67ccB19297":   true}
	for addr, paas := range testcase {
		_, err := ParseAddress(addr)
		assert.Equal(t, paas, err == nil)
	}
}

func TestParseAddressStrict(t *testing.T) {
	testcase := map[string]bool{
		"0x388C818CA8B9251b393131C08a736A67ccB19297": true,
		"0x388c818CA8B9251b393131C08a736A67ccB19297": false,
		"0x388C818Ca8B9251b393131C08a736A67ccB1929x": false,
		"388C818CA8B9251b393131C08a736A67ccB19297":   false}
	for addr, paas := range testcase {
		_, err := ParseAddressStrict(addr)
		assert.Equal(t, paas, err == nil, addr)
	}
}
func TestIsZeroAddress(t *testing.T) {
	emptAddr := StringToAddress("")
	assert.True(t, emptAddr.IsZero())
	assert.True(t, IsZeroAddress(""))
	assert.True(t, IsZeroAddress("0xabcdfegsdfx"))
}

func TestAddressToJson(t *testing.T) {
	addr, _ := ParseAddress("0x388C818CA8B9251b393131C08a736A67ccB19297")
	j, err := json.Marshal(addr)
	assert.NoError(t, err)
	t.Log(string(j))

	addr2 := ZeroAddress
	json.Unmarshal(j, &addr2)
	t.Log(addr2.String())
}

type KA struct {
	K string
	A Address
}

func TestAddrJson(t *testing.T) {
	addr, _ := ParseAddress("0x388C818CA8B9251b393131C08a736A67ccB19297")
	kv := &KA{
		K: "test",
		A: addr,
	}
	j, err := json.Marshal(kv)
	assert.NoError(t, err)
	t.Log(string(j))
}
