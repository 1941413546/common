/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package ethbase

import (
	"fmt"
	"math/big"

	"chainmaker.org/chainmaker/common/v3/ethbase/keccak"
	"chainmaker.org/chainmaker/common/v3/json"
	"github.com/umbracle/fastrlp"
)

type LegacyTransaction struct {
	Nonce    uint64
	GasPrice *big.Int
	Gas      uint64
	To       *Address
	Value    *big.Int
	Input    []byte
	V        *big.Int
	R        *big.Int
	S        *big.Int
	//Hash     Hash
	//From     Address
	//
	//// Cache
	//size atomic.Value
}

func (t *LegacyTransaction) GetSignatureYParity() byte {
	v := t.V.Uint64() % 2
	return byte(v)
}

func (t *LegacyTransaction) GetTxType() TxType {
	return LegacyTxType
}

func (t *LegacyTransaction) GetChainID() *big.Int {
	v := t.V
	if v.BitLen() <= 64 {
		v := v.Uint64()
		if v == 27 || v == 28 {
			return new(big.Int)
		}
		return new(big.Int).SetUint64((v - 35) / 2)
	}
	v = new(big.Int).Sub(v, big.NewInt(35))
	return v.Div(v, big.NewInt(2))
}

func (t *LegacyTransaction) GetAccessList() AccessList {
	return nil
}

func (t *LegacyTransaction) GetData() []byte {
	return t.Input
}

func (t *LegacyTransaction) GetGas() uint64 {
	return t.Gas
}

func (t *LegacyTransaction) GetGasPrice() *big.Int {
	return t.GasPrice
}

func (t *LegacyTransaction) GetGasTipCap() *big.Int {
	return t.GasPrice
}

func (t *LegacyTransaction) GetGasFeeCap() *big.Int {
	return t.GasPrice
}

func (t *LegacyTransaction) GetValue() *big.Int {
	return t.Value
}

func (t *LegacyTransaction) GetNonce() uint64 {
	return t.Nonce
}

func (t *LegacyTransaction) GetTo() *Address {
	return t.To
}

var marshalArenaPool fastrlp.ArenaPool

func (t *LegacyTransaction) IsContractCreation() bool {
	return t.To == nil
}

// ComputeHash computes the hash of the transaction
func (t *LegacyTransaction) ComputeHash() Hash {
	ar := marshalArenaPool.Get()
	hash := keccak.DefaultKeccakPool.Get()
	h := Hash{}
	v := t.MarshalRLPWith(ar)
	hash.WriteRlp(h[:0], v)

	marshalArenaPool.Put(ar)
	keccak.DefaultKeccakPool.Put(hash)

	return h
}

func (t *LegacyTransaction) Copy() *LegacyTransaction {
	tt := new(LegacyTransaction)
	*tt = *t

	tt.GasPrice = new(big.Int)
	if t.GasPrice != nil {
		tt.GasPrice.Set(t.GasPrice)
	}

	tt.Value = new(big.Int)
	if t.Value != nil {
		tt.Value.Set(t.Value)
	}

	if t.R != nil {
		tt.R = new(big.Int)
		tt.R = big.NewInt(0).SetBits(t.R.Bits())
	}

	if t.S != nil {
		tt.S = new(big.Int)
		tt.S = big.NewInt(0).SetBits(t.S.Bits())
	}

	tt.Input = make([]byte, len(t.Input))
	copy(tt.Input[:], t.Input[:])

	return tt
}

// Cost returns gas * gasPrice + value
//func (t *LegacyTransaction) Cost() *big.Int {
//	total := new(big.Int).Mul(t.GasPrice, new(big.Int).SetUint64(t.Gas))
//	total.Add(total, t.Value)
//
//	return total
//}

//func (t *LegacyTransaction) Size() uint64 {
//	if size := t.size.Load(); size != nil {
//		sizeVal, ok := size.(uint64)
//		if !ok {
//			return 0
//		}
//
//		return sizeVal
//	}
//
//	size := uint64(len(t.MarshalRLP()))
//	t.size.Store(size)
//
//	return size
//}

//func (t *LegacyTransaction) ExceedsBlockGasLimit(blockGasLimit uint64) bool {
//	return t.Gas > blockGasLimit
//}
//
//func (t *LegacyTransaction) IsUnderpriced(priceLimit uint64) bool {
//	return t.GasPrice.Cmp(big.NewInt(0).SetUint64(priceLimit)) < 0
//}

type marshalRLPFunc func(ar *fastrlp.Arena) *fastrlp.Value

func MarshalRLPTo(obj marshalRLPFunc, dst []byte) []byte {
	ar := fastrlp.DefaultArenaPool.Get()
	dst = obj(ar).MarshalTo(dst)
	fastrlp.DefaultArenaPool.Put(ar)

	return dst
}

func (t *LegacyTransaction) MarshalRLP() []byte {
	return t.MarshalRLPTo(nil)
}

func (t *LegacyTransaction) MarshalRLPTo(dst []byte) []byte {
	return MarshalRLPTo(t.MarshalRLPWith, dst)
}

// MarshalRLPWith marshals the transaction to RLP with a specific fastrlp.Arena
func (t *LegacyTransaction) MarshalRLPWith(arena *fastrlp.Arena) *fastrlp.Value {
	vv := arena.NewArray()

	vv.Set(arena.NewUint(t.Nonce))
	vv.Set(arena.NewBigInt(t.GasPrice))
	vv.Set(arena.NewUint(t.Gas))

	// Address may be empty
	if t.To != nil {
		vv.Set(arena.NewBytes((*t.To).Bytes()))
	} else {
		vv.Set(arena.NewNull())
	}

	vv.Set(arena.NewBigInt(t.Value))
	vv.Set(arena.NewCopyBytes(t.Input))

	// signature values
	vv.Set(arena.NewBigInt(t.V))
	vv.Set(arena.NewBigInt(t.R))
	vv.Set(arena.NewBigInt(t.S))

	return vv
}

type unmarshalRLPFunc func(p *fastrlp.Parser, v *fastrlp.Value) error

func UnmarshalRlp(obj unmarshalRLPFunc, input []byte) error {
	pr := fastrlp.DefaultParserPool.Get()

	v, err := pr.Parse(input)
	if err != nil {
		fastrlp.DefaultParserPool.Put(pr)

		return err
	}

	if err := obj(pr, v); err != nil {
		fastrlp.DefaultParserPool.Put(pr)

		return err
	}

	fastrlp.DefaultParserPool.Put(pr)

	return nil
}
func (t *LegacyTransaction) UnmarshalRLP(input []byte) error {
	return UnmarshalRlp(t.UnmarshalRLPFrom, input)
}

// UnmarshalRLPFrom unmarshals a LegacyTransaction in RLP format
func (t *LegacyTransaction) UnmarshalRLPFrom(p *fastrlp.Parser, v *fastrlp.Value) error {
	elems, err := v.GetElems()
	if err != nil {
		return err
	}

	if len(elems) < 9 {
		return fmt.Errorf("incorrect number of elements to decode transaction, expected 9 but found %d", len(elems))
	}

	//p.Hash(t.Hash[:0], v)

	// nonce
	if t.Nonce, err = elems[0].GetUint64(); err != nil {
		return err
	}
	// gasPrice
	t.GasPrice = new(big.Int)
	if err := elems[1].GetBigInt(t.GasPrice); err != nil {
		return err
	}
	// gas
	if t.Gas, err = elems[2].GetUint64(); err != nil {
		return err
	}
	// to
	if vv, _ := v.Get(3).Bytes(); len(vv) == 20 {
		// address
		addr := BytesToAddress(vv)
		t.To = &addr
	} else {
		// reset To
		t.To = nil
	}
	// value
	t.Value = new(big.Int)
	if err := elems[4].GetBigInt(t.Value); err != nil {
		return err
	}
	// input
	if t.Input, err = elems[5].GetBytes(t.Input[:0]); err != nil {
		return err
	}

	// V
	t.V = new(big.Int)
	if err = elems[6].GetBigInt(t.V); err != nil {
		return err
	}

	// R
	t.R = new(big.Int)
	if err = elems[7].GetBigInt(t.R); err != nil {
		return err
	}
	// S
	t.S = new(big.Int)
	if err = elems[8].GetBigInt(t.S); err != nil {
		return err
	}

	return nil
}

// GetSignature 获得RSV拼接后的签名数组，v已经去掉了chainId
// @return []byte
// @return error
func (t *LegacyTransaction) GetSignature() ([]byte, error) {

	v := t.V.Uint64()
	if v == 27 || v == 28 {
		v = v - 27
	} else {
		//v = CHAIN_ID * 2 + 35 + {0, 1}
		v = (v + 1) % 2
	}
	return EncodeSignature(t.R, t.S, byte(v))
}

var emptyFrom = Address{}

func (t *LegacyTransaction) GetSigner() (Address, error) {
	signer := NewSigner(t.GetChainID().Uint64())
	// Decrypt the from address
	return signer.Sender(t)
}
func (t *LegacyTransaction) GetUnsignedHash() Hash {
	return calcTxHash(t, t.GetChainID().Uint64())
}
func (t *LegacyTransaction) GetRawSignatureValues() (r, s, v *big.Int) {
	return t.R, t.S, t.V
}

var signerPool fastrlp.ArenaPool

// calcTxHash calculates the transaction hash (keccak256 hash of the RLP value)
func calcTxHash(tx *LegacyTransaction, chainID uint64) Hash {
	a := signerPool.Get()

	v := a.NewArray()
	v.Set(a.NewUint(tx.Nonce))
	v.Set(a.NewBigInt(tx.GasPrice))
	v.Set(a.NewUint(tx.Gas))

	if tx.To == nil {
		v.Set(a.NewNull())
	} else {
		v.Set(a.NewCopyBytes((*tx.To).Bytes()))
	}

	v.Set(a.NewBigInt(tx.Value))
	v.Set(a.NewCopyBytes(tx.Input))

	// EIP155
	if chainID != 0 {
		v.Set(a.NewUint(chainID))
		v.Set(a.NewUint(0))
		v.Set(a.NewUint(0))
	}

	hash := keccak.Keccak256Rlp(nil, v)

	signerPool.Put(a)

	return BytesToHash(hash)
}
func (t *LegacyTransaction) ToString() string {
	d, _ := json.Marshal(t)
	return string(d)
}
