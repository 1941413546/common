/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package bitmap

import (
	"bytes"
	"fmt"
)

//Bitmap 位图对象
type Bitmap struct {
	words []uint64
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func (bitmap *Bitmap) fillLen(length int) {
	for len(bitmap.words) < length {
		bitmap.words = append(bitmap.words, 0)
	}
}

// Has 判断位图中num是否存在
// @param num
// @return bool
func (bitmap *Bitmap) Has(num int) bool {
	word, bit := num/64, uint(num%64)
	return word < len(bitmap.words) && (bitmap.words[word]&(1<<bit)) != 0
}

// Set 设置num到位图
// @param num
// @return *Bitmap
func (bitmap *Bitmap) Set(num int) *Bitmap {
	word, bit := num/64, uint(num%64)
	for word >= len(bitmap.words) {
		bitmap.words = append(bitmap.words, 0)
	}
	// 判断num是否已经存在bitmap中
	if bitmap.words[word]&(1<<bit) == 0 {
		bitmap.words[word] |= 1 << bit
	}
	return bitmap
}

// InterExist 一个位图是否在另一个位图中存在
// @param bitmap2
// @return bool
func (bitmap *Bitmap) InterExist(bitmap2 *Bitmap) bool {
	if bitmap2 == nil {
		return false
	}
	for i := 0; i < min(len(bitmap.words), len(bitmap2.words)); i++ {
		if bitmap.words[i]&bitmap2.words[i] > 0 {
			return true
		}
	}
	return false
}

// Or 或操作两个位图对象
// @param bitmap2
func (bitmap *Bitmap) Or(bitmap2 *Bitmap) {
	if bitmap2 == nil {
		return
	}
	bitmap.fillLen(len(bitmap2.words))
	for i := 0; i < len(bitmap2.words); i++ {
		bitmap.words[i] = bitmap.words[i] | bitmap2.words[i]
	}
}

// Xor 异或两个位图对象
// @param bitmap2
func (bitmap *Bitmap) Xor(bitmap2 *Bitmap) {
	if bitmap2 == nil {
		return
	}
	bitmap.fillLen(len(bitmap2.words))
	for i := 0; i < len(bitmap2.words); i++ {
		bitmap.words[i] = bitmap.words[i] ^ bitmap2.words[i]
	}
}

// Clone 克隆对象
// @return *Bitmap
func (bitmap *Bitmap) Clone() *Bitmap {
	bitmap2 := &Bitmap{
		words: make([]uint64, len(bitmap.words)),
	}
	copy(bitmap2.words, bitmap.words)
	return bitmap2
}

// Pos1 所有为1的Pos
func (bitmap *Bitmap) Pos1() []int {
	var pos []int
	for i, v := range bitmap.words {
		if v == 0 {
			continue
		}
		for j := uint(0); j < 64; j++ {
			if v&(1<<j) != 0 {
				pos = append(pos, 64*i+int(j))
			}
		}
	}
	return pos
}

func (bitmap *Bitmap) String() string {
	var buf bytes.Buffer
	buf.WriteByte('{')
	for i, v := range bitmap.words {
		if v == 0 {
			continue
		}
		for j := uint(0); j < 64; j++ {
			if v&(1<<j) != 0 {
				if buf.Len() > len("{") {
					buf.WriteByte(' ')
				}
				fmt.Fprintf(&buf, "%d", 64*i+int(j))
			}
		}
	}
	buf.WriteByte('}')
	return buf.String()
}
