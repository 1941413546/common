/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

// nolint
package tencentcloudkms

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"testing"

	"github.com/tjfoc/gmsm/sm2"

	"github.com/tjfoc/gmsm/sm3"

	bccrypto "chainmaker.org/chainmaker/common/v3/crypto"
	"github.com/stretchr/testify/require"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common"
	"github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/common/errors"
	kms "github.com/tencentcloud/tencentcloud-sdk-go/tencentcloud/kms/v20190118"
)

var (
	msgConst = "Valar morgulis."
	isPublic = true
)

const (
	kmsServer = "kms.tencentcloudapi.com"
	kmsRegion = "ap-guangzhou"
	secretId  = "AKIDA9uPef95S0JOHQx0RzCF3qPilYUfrjNm"
	secretKey = "VllDRRBfCTVHn46sNYUKSqysWhdR0K0T"
	sm2KeyId  = "e2920cd5-5a02-11eb-840b-525400e8e6ea"
)

//const (
//	kmsServer = "124.127.49.181:19080"
//	kmsRegion = "ap-beijing"
//	secretId  = "AKCACCAGAGBFFFBHCQUM"
//	secretKey = "MFRESFI2UjFYMUNVSjA4Nw=="
//	sm2KeyId  = "9563e338-c535-4e97-947b-41218b8d2575" // 签名和验签
//)

func verifyKMS(msg string, sig []byte, sk *PrivateKey, client *kms.Client) (bool, error) {
	sigBase64 := base64.StdEncoding.EncodeToString(sig)
	msgBase64 := base64.StdEncoding.EncodeToString([]byte(msg))

	request := kms.NewVerifyByAsymmetricKeyRequest()

	request.KeyId = common.StringPtr(sk.keyId)
	request.SignatureValue = common.StringPtr(sigBase64)
	request.Message = common.StringPtr(msgBase64)
	request.Algorithm = common.StringPtr(sk.keyType)
	request.MessageType = common.StringPtr(MODE_DIGEST)

	response, err := client.VerifyByAsymmetricKey(request)
	if _, ok := err.(*errors.TencentCloudSDKError); ok {
		return false, fmt.Errorf("An API error has returned: %s", err)
	}
	if err != nil {
		return false, err
	}

	return *(response.Response.SignatureValid), nil
}

func TestKMS(t *testing.T) {
	kmsConfig := &KMSConfig{
		SecretId:      secretId,
		SecretKey:     secretKey,
		ServerAddress: kmsServer,
		ServerRegion:  kmsRegion,
	}

	signOpts := &bccrypto.SignOpts{
		Hash: bccrypto.HASH_TYPE_SM3,
		UID:  bccrypto.CRYPTO_DEFAULT_UID,
	}

	client, err := CreateConnection(kmsConfig)
	require.Nil(t, err)

	keyConfig := &KMSPrivateKeyConfig{
		KeyType:  bccrypto.CRYPTO_ALGO_SM2,
		KeyId:    sm2KeyId,
		KeyAlias: "",
	}
	sk, err := NewPrivateKey(client, keyConfig, isPublic)
	require.Nil(t, err)

	sig, err := sk.SignWithOpts([]byte(msgConst), signOpts)
	require.Nil(t, err)

	pkSM2, _ := sk.PublicKey().ToStandardKey().(*sm2.PublicKey)
	uid := bccrypto.CRYPTO_DEFAULT_UID
	za, _ := sm2.ZA(pkSM2, []byte(uid))
	e := sm3.New()
	e.Write(za)
	e.Write([]byte(msgConst))
	dgst := e.Sum(nil)[:32]
	isValidKMS, err := verifyKMS(string(dgst), sig, sk.(*PrivateKey), client)
	require.Nil(t, err)
	require.Equal(t, true, isValidKMS)

	fmt.Println(hex.EncodeToString(sig))

	isValid, err := sk.PublicKey().VerifyWithOpts([]byte(msgConst), sig, signOpts)
	require.Nil(t, err)
	require.Equal(t, true, isValid)

	//skInfo, err := sk.String()
	//require.Nil(t, err)

	//skLoaded, err := LoadPrivateKey(client, []byte(skInfo))
	//require.Nil(t, err)
	//
	//sig, err = skLoaded.SignWithOpts([]byte(msgConst), signOpts)
	//require.Nil(t, err)
	//
	//isValidKMS, err = verifyKMS(msgConst, sig, skLoaded.(*PrivateKey), client)
	//require.Nil(t, err)
	//require.Equal(t, true, isValidKMS)
	//
	//isValid, err = skLoaded.PublicKey().VerifyWithOpts([]byte(msgConst), sig, signOpts)
	//require.Nil(t, err)
	//require.Equal(t, true, isValid)
	//
	//isValidKMS, err = verifyKMS(msgConst, sig, sk.(*PrivateKey), client)
	//require.Nil(t, err)
	//require.Equal(t, true, isValidKMS)
	//
	//isValid, err = sk.PublicKey().VerifyWithOpts([]byte(msgConst), sig, signOpts)
	//require.Nil(t, err)
	//require.Equal(t, true, isValid)
	//
	//signer := &Signer{skLoaded.(*PrivateKey)}
	//
	//sig, err = signer.Sign(nil, []byte(msgConst), nil)
	//require.Nil(t, err)
	//
	//isValid, err = sk.PublicKey().VerifyWithOpts([]byte(msgConst), sig, signOpts)
	//require.Nil(t, err)
	//require.Equal(t, true, isValid)
	//
	//fmt.Printf("KMS private key serialization: %s\n", skInfo)
	//
	//fmt.Println("KMS test done")
}
