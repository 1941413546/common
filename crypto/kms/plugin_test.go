/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package kms

import (
	"math/big"
	"testing"

	bccrypto "chainmaker.org/chainmaker/common/v3/crypto"
	"github.com/stretchr/testify/assert"
)

type Sig struct {
	R *big.Int
	S *big.Int
}

func TestGetHSMAdapter(t *testing.T) {
	//os.Setenv("KMS_ADAPTER_LIB", "./tencentcloudkms/adapter.so")
	sm2KeyId := "e2920cd5-5a02-11eb-840b-525400e8e6ea"
	msg := "Valar morgulis."

	adapter := GetKMSAdapter(&Config{
		SecretId:  "AKIDA9uPef95S0JOHQx0RzCF3qPilYUfrjNm",
		SecretKey: "VllDRRBfCTVHn46sNYUKSqysWhdR0K0T",
		Address:   "kms.tencentcloudapi.com",
		Region:    "ap-guangzhou",
		SDKScheme: "https",
		IsPublic:  true,
	})

	//sm2KeyId = "9e375de6-6070-4c84-aefc-926dc20addcc"
	//adapter := GetKMSAdapter(&Config{
	//	SecretId:  "AKCACCAGBDBEADDJNOBG",
	//	SecretKey: "Tk5UTE01UlM1WDgwNlFTMA==",
	//	Address:   "124.127.49.181:19080",
	//	Region:    "ap-beijing",
	//	SDKScheme: "http",
	//	IsPublic:  false,
	//})
	assert.NotNil(t, adapter)

	sk, err := adapter.NewPrivateKey(PrivateKey{
		KeyId:    sm2KeyId,
		KeyType:  "SM2DSA",
		KeyAlias: "",
	})
	assert.NoError(t, err)

	//for i := 0; i < 1000; i++ {
	sig, err := sk.SignWithOpts([]byte(msg), &bccrypto.SignOpts{Hash: bccrypto.HASH_TYPE_SM3, UID: bccrypto.CRYPTO_DEFAULT_UID})
	assert.NoError(t, err)

	pk, err := adapter.NewPublicKey(sm2KeyId)
	assert.NoError(t, err)
	ok, err := pk.VerifyWithOpts([]byte(msg), sig, &bccrypto.SignOpts{Hash: bccrypto.HASH_TYPE_SM3, UID: bccrypto.CRYPTO_DEFAULT_UID})
	assert.NoError(t, err)
	assert.True(t, ok)

	//	//if err != nil {
	//	skBytes, _ := sk.Bytes()
	//	pkBytes, _ := pk.Bytes()
	//	fmt.Printf("i = %d\nsk = %s\npk = %s\nmsg =%s\nsig = %s\n", i, hex.EncodeToString(skBytes),
	//		hex.EncodeToString(pkBytes),
	//		hex.EncodeToString([]byte(msg)),
	//		hex.EncodeToString(sig))
	//	//}
	//}
}
