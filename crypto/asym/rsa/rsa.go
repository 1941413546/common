/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package rsa

const (
	//RSA_OAEP rsa OAEP padding mode
	RSA_OAEP = "OAEP"
	// RSA_PKCS1 rsa pkcs1 padding mode
	RSA_PKCS1 = "PKCS1"
	// RSA_PSS rsa pss padding mode
	RSA_PSS = "PSS"
)
