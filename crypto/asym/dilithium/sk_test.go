package dilithium

import (
	"crypto"
	"testing"

	bccrypto "chainmaker.org/chainmaker/common/v3/crypto"
	"github.com/stretchr/testify/assert"
)

var (
	msg = []byte("hello dilithium")
)

func TestVerify(t *testing.T) {
	key, err := New(bccrypto.DILITHIUM2)
	assert.NoError(t, err)
	assert.NotNil(t, key)

	sig, err := key.SignWithOpts(msg, &bccrypto.SignOpts{Hash: bccrypto.HASH_TYPE_SHA3_256})
	assert.NoError(t, err)
	assert.NotNil(t, sig)

	ok, err := key.PublicKey().VerifyWithOpts(msg, sig, &bccrypto.SignOpts{Hash: bccrypto.HASH_TYPE_SHA3_256})
	assert.NoError(t, err)
	assert.True(t, ok)

	//test crypto.Signer
	sig, err = key.ToStandardKey().(crypto.Signer).Sign(nil, msg, nil)
	assert.NoError(t, err)
	assert.NotNil(t, sig)

	ok, err = key.PublicKey().VerifyWithOpts(msg, sig, nil)
	assert.NoError(t, err)
	assert.True(t, ok)
}
