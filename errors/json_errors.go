/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package errors

import (
	"fmt"

	"chainmaker.org/chainmaker/common/v3/json"
)

var (
	// json
	// -32768 to -32000 is the reserved predefined error code

	//ErrParseError the server received an invalid JSON. The error is sent to the server trying to parse the JSON text
	ErrParseError = JsonError{Code: -32700, Message: "Parse error"}
	//ErrInvalidRequest the sent JSON is not a valid request object
	ErrInvalidRequest = JsonError{Code: -32600, Message: "Invalid request"}
	//ErrMethodNotFound the method does not exist or is invalid
	ErrMethodNotFound = JsonError{Code: -32601, Message: "Method not found"}
	//ErrInvalidParams invalid method parameter
	ErrInvalidParams = JsonError{Code: -32602, Message: "Invalid params"}
	//ErrInternalError json-rpc internal error.
	ErrInternalError = JsonError{Code: -32603, Message: "Internal error"}
	// -32000 to -32099	is the server error reserved for customization

	// txPool

	//ErrStructEmpty the object is nil
	ErrStructEmpty = JsonError{Code: -31100, Message: "Struct is nil"}
	//ErrTxIdExist tx-id already exists
	ErrTxIdExist = JsonError{Code: -31105, Message: "TxId exist"}
	//ErrTxIdExistDB tx-id already exists in DB
	ErrTxIdExistDB = JsonError{Code: -31106, Message: "TxId exist in DB"}
	//ErrTxTimeout tx-timestamp out of range
	ErrTxTimeout = JsonError{Code: -31108, Message: "TxTimestamp error"}
	//ErrTxPoolLimit transaction pool is full
	ErrTxPoolLimit = JsonError{Code: -31110, Message: "TxPool is full"}
	//ErrTxSource tx-source is error
	ErrTxSource = JsonError{Code: -31112, Message: "TxSource is err"}
	//ErrTxHadOnTheChain The tx had been on the block chain
	ErrTxHadOnTheChain = JsonError{Code: -31113, Message: "The tx had been on the block chain"}
	//ErrTxPoolHasStopped The txPool service has stopped
	ErrTxPoolHasStopped = JsonError{Code: -31114, Message: "The tx pool has stopped"}
	//ErrTxPoolHasStarted The txPool service has started
	ErrTxPoolHasStarted = JsonError{Code: -31115, Message: "The tx pool has started"}
	//ErrTxPoolStopFailed The txPool service stop failed
	ErrTxPoolStopFailed = JsonError{Code: -31116, Message: "The tx pool stop failed"}
	//ErrTxPoolStartFailed The txPool service start failed
	ErrTxPoolStartFailed = JsonError{Code: -31117, Message: "The tx pool start failed"}

	// core

	//ErrBlockHadBeenCommitted block had been committed
	ErrBlockHadBeenCommitted = JsonError{Code: -31200, Message: "Block had been committed err"}
	//ErrConcurrentVerify block concurrent verify error
	ErrConcurrentVerify = JsonError{Code: -31201, Message: "Block concurrent verify err"}
	//ErrRepeatedVerify block had been verified
	ErrRepeatedVerify = JsonError{Code: -31202, Message: "Block had been verified err"}

	// sync

	//ErrSyncServiceHasStarted The sync service has been started
	ErrSyncServiceHasStarted = JsonError{Code: -33000, Message: "The sync service has been started"}
	//ErrSyncServiceHasStopped The sync service has been stoped
	ErrSyncServiceHasStopped = JsonError{Code: -33001, Message: "The sync service has been stopped"}

	// store

	//ErrStoreServiceNeedRestarted The store service has been started
	ErrStoreServiceNeedRestarted = JsonError{Code: -34000, Message: "The store service need restart"}
)

// core
var (
	//WarnRwSetVerifyFailTxs Block rw set verify fail txs
	WarnRwSetVerifyFailTxs = JsonError{Code: -31203, Message: "Block rw set verify fail txs"}
)

// JsonError Json返回的错误
type JsonError struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data,omitempty"`
}

// String 获得错误的字符串
// @return string
func (err JsonError) String() string {
	marshal, _ := json.Marshal(err)
	return string(marshal)
}

// Error 获得错误的Message
// @return string
func (err JsonError) Error() string {
	if err.Message == "" {
		return fmt.Sprintf("error %d", err.Code)
	}
	return err.Message
}

// ErrorCode 获得错误的Code
// @return int
func (err JsonError) ErrorCode() int {
	return err.Code
}
